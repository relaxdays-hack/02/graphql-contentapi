﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MaxLib.WebServer;
using MaxLib.WebServer.Services;
using Serilog;
using Serilog.Events;

namespace GraphQlContentApi
{
    class Program
    {
        public static Dictionary<string, float> Conversion { get; private set; }
            = new Dictionary<string, float>();

#pragma warning disable CS8618
        public static DB DB { get; private set; }
#pragma warning restore CS8618

        public static string[] Languages = new[]
        {
            "DE", "EN", "AR", "ZH", "NL", "FI", "FR", "HI", "HU", "ID", "GA",
            "IT", "JA", "KO", "PL", "PT", "RU", "ES", "SV", "TR", "UK", "VI"
        };

        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Console(LogEventLevel.Verbose,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
            WebServerLog.LogPreAdded += WebServerLog_LogPreAdded;

            DB = new DB();

            await FetchPrices();

            var isDocker = Environment.GetEnvironmentVariable("IS_DOCKER") == "1";
            var server = new Server(new WebServerSettings(isDocker ? 80 : 8000, 5000));
            server.AddWebService(new HttpRequestParser());
            server.AddWebService(new HttpHeaderSpecialAction());
            server.AddWebService(new Http404Service());
            server.AddWebService(new HttpResponseCreator());
            server.AddWebService(new HttpSender());
            server.AddWebService(new GraphQlService());

            server.Start();

            await Task.Delay(-1).ConfigureAwait(false);
        }

        private static readonly MessageTemplate serilogMessageTemplate =
            new Serilog.Parsing.MessageTemplateParser().Parse(
                "{infoType}: {info}"
            );

        private static void WebServerLog_LogPreAdded(ServerLogArgs e)
        {
            e.Discard = true;
            Log.Write(new LogEvent(
                e.LogItem.Date,
                e.LogItem.Type switch
                {
                    ServerLogType.Debug => LogEventLevel.Verbose,
                    ServerLogType.Information => LogEventLevel.Debug,
                    ServerLogType.Error => LogEventLevel.Error,
                    ServerLogType.FatalError => LogEventLevel.Fatal,
                    _ => LogEventLevel.Information,
                },
                null,
                serilogMessageTemplate,
                new[]
                {
                        new LogEventProperty("infoType", new ScalarValue(e.LogItem.InfoType)),
                        new LogEventProperty("info", new ScalarValue(e.LogItem.Information))
                }
            ));
        }

        private static async Task FetchPrices()
        {
            string doc;
            if (System.IO.File.Exists("db/currency.xml") 
                && System.IO.File.GetLastWriteTimeUtc("db/currency.xml").Date == DateTime.UtcNow.Date)
            {
                doc = await System.IO.File.ReadAllTextAsync("db/currency.xml")
                    .ConfigureAwait(false);
            }
            else
            {
                var wc = new System.Net.WebClient();
                doc = await wc.DownloadStringTaskAsync("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
                    .ConfigureAwait(false);
                await System.IO.File.WriteAllTextAsync("db/currency.xml", doc)
                    .ConfigureAwait(false);
            }
            var prices = new Dictionary<string, float>();
            var culture = System.Globalization.CultureInfo.InvariantCulture;
            var regex = new System.Text.RegularExpressions.Regex(
                "currency='(\\w+)' rate='(\\d+.\\d+)'"
            );
            foreach (System.Text.RegularExpressions.Match match in regex.Matches(doc))
            {
                var number = float.Parse(match.Groups[2].Value, culture.NumberFormat);
                prices[match.Groups[1].Value] = number;
            }
            Conversion = prices;
            Serilog.Log.Information("Prices updated");
        }
    }
}

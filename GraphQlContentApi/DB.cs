using System;
using System.Collections.Generic;
using GraphQlContentApi.Schema;
using LiteDB;
using System.Linq;

namespace GraphQlContentApi
{
    public class DB
    {
        private readonly LiteDatabase database;
        public ILiteCollection<Article> Articles { get; }

        public ILiteCollection<Variant> Variants { get; }

        public DB()
        {
            database = new LiteDatabase("db/database.db");
            Articles = database.GetCollection<Article>("articles");
            Articles.EnsureIndex(x => x.EAN, true);

            Variants = database.GetCollection<Variant>("variants");

            BsonMapper.Global.Entity<Article>()
                .DbRef(x => x.Variants, "variants");
        }

        public Article? GetArticle(ObjectId id)
        {
            return Articles.Query()
                .Include(x => x.Variants)
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public Article? GetArticle(string ean)
        {
            return Articles.Query()
                .Include(x => x.Variants)
                .Where(x => x.EAN == ean)
                .FirstOrDefault();
        }

        public List<Article> FindArticles(FieldOrdering order, ArticleFilterInput where)
        {
            return Articles.Query()
                .Include(x => x.Variants)
                .Order(order)
                .ToEnumerable()
                .Where(x => where.Filter(x))
                .ToList();
        }

        public Pagination<T> FindPagination<T>(int? skip, int? take, List<T> input)
        {
            skip ??= 0;
            take ??= input.Count - skip;
            var start = Math.Clamp(skip.Value, 0, input.Count);
            var end = Math.Clamp(start + take.Value, 0, input.Count);
            return new Pagination<T>()
            {
                Items = input.GetRange(start, end-start),
                PageInfo = new PageInfo
                {
                    HasPreviousPage = skip - take >= 0 && skip - take < input.Count,
                    HasNextPage = skip + take < input.Count && skip + take >= 0,
                },
                TotalCount = input.Count,
            };
        }

        public Article AddArticle(ArticleInput input, string? user)
        {
            if (GetArticle(input.EAN) is not null)
                throw new ArgumentException($"ean {input.EAN} already exists");
            var article = new Article
            {
                EAN = input.EAN,
                Titles = new Dictionary<string, Translation>
                {
                    {
                        "DE",
                        new Translation
                        {
                            Content = input.Title ?? "",
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "title",
                            Language = "DE",
                            ManuallyTranslated = true,
                        }
                    }
                },
                Descriptions = new Dictionary<string, Translation>
                {
                    {
                        "DE",
                        new Translation
                        {
                            Content = input.Description ?? "",
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "description",
                            Language = "DE",
                            ManuallyTranslated = true,
                        }
                    }
                },
                Price = input.Price ?? 0,
                CreatedBy = user,
                CreatedAt = DateTime.UtcNow,
                LastChangedBy = user,
                LastChangedAt = DateTime.UtcNow,
            };
            Articles.Insert(article);
            article.Titles["DE"].Article = article;
            article.Descriptions["DE"].Article = article;
            Articles.Update(article);
            return article;
        }

        public Article ChangeArticle(ArticleInput input, string? user)
        {
            var article = GetArticle(input.EAN);
            if (article is null)
                throw new KeyNotFoundException($"ean {input.EAN} not found");
            if (input.Title is not null)
            {
                article.Titles["DE"].Content = input.Title ?? "";
                article.Titles["DE"].CreatedAt = DateTime.UtcNow;
                article.Titles["DE"].CreatedBy = user;
            }
            if (input.Description is not null)
            {
                article.Titles["DE"].Content = input.Description ?? "";
                article.Titles["DE"].CreatedAt = DateTime.UtcNow;
                article.Titles["DE"].CreatedBy = user;
            }
            if (input.Price is not null)
            {
                article.Price = input.Price ?? 0;
            }
            article.LastChangedAt = DateTime.UtcNow;
            article.LastChangedBy = user;
            Articles.Update(article);
            return article;
        }

        public Variant AddVariant(VariantInput input, string? user)
        {
            var article = GetArticle(input.Parent.EAN);
            if (article is null)
                throw new ArgumentException($"parent ean {input.Parent.EAN} not found");
            foreach (var v in article.Variants)
                if (v.Characteristics["DE"].Content == input.Characteristic)
                    throw new ArgumentException($"characteristic {input.Characteristic} already found");
            var variant = new Variant
            {
                Characteristics = new Dictionary<string, Translation>
                {
                    {
                        "DE",
                        new Translation
                        {
                            Content = input.Characteristic,
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "characteristic",
                            Language = "DE",
                            ManuallyTranslated = true,
                        }
                    }
                },
                Parent = article,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = user,
                LastChangedAt = DateTime.UtcNow,
                LastChangedBy = user,
            };
            Variants.Insert(variant);
            variant.Characteristics["DE"].Variant = variant;
            Variants.Update(variant);
            article.Variants.Add(variant);
            article.LastChangedAt = DateTime.UtcNow;
            article.LastChangedBy = user;
            Articles.Update(article);
            return variant;
        }

        public Article DeleteVariant(VariantInput input, string? user)
        {
            var article = GetArticle(input.Parent.EAN);
            if (article is null)
                throw new ArgumentException($"parent ean {input.Parent.EAN} not found");
            foreach (var v in article.Variants)
                if (v.Characteristics["DE"].Content == input.Characteristic)
                {
                    Variants.Delete(v.Id);
                    article.Variants.Remove(v);
                    break;
                }
            article.LastChangedAt = DateTime.UtcNow;
            article.LastChangedBy = user;
            Articles.Update(article);
            return article;
        }

        public Article DeleteVariant(int id, string? user)
        {
            var variant = Variants.FindById(id);
            if (variant is null)
                throw new ArgumentException("variant not found");
            Variants.Delete(id);
            var article = GetArticle(variant.Parent.Id);
            if (article is null)
                throw new InvalidOperationException("article should exists");
            article.Variants.RemoveAll(x => x.Id == id);
            article.LastChangedAt = DateTime.UtcNow;
            article.LastChangedBy = user;
            Articles.Update(article);
            return article;
        }

        public Translation AddTranslation(TranslationInput input, string? user)
        {
            Translation translation;
            if (input.Article is not null)
            {
                if (input.Variant is not null)
                    throw new ArgumentException("translation cannot bound to article and variant");
                var article = GetArticle(input.Article.EAN);
                if (article is null)
                    throw new ArgumentException($"article with ean {input.Article.EAN} cannot found");
                switch (input.Field.ToLower())
                {
                    case "title":
                        article.Titles[input.Language.ToUpper()] = translation = new Translation
                        {
                            Article = article,
                            Content = input.Content,
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "title",
                            Language = input.Language.ToUpper(),
                            ManuallyTranslated = true,
                        };
                        break;
                    case "description":
                        article.Descriptions[input.Language.ToUpper()] = translation = new Translation
                        {
                            Article = article,
                            Content = input.Content,
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "description",
                            Language = input.Language.ToUpper(),
                            ManuallyTranslated = true,
                        };
                        break;
                    default: throw new ArgumentException($"article has no field {input.Field}");
                }
                article.LastChangedAt = DateTime.UtcNow;
                article.LastChangedBy = user;
                Articles.Update(article);
            }
            else if (input.Variant is not null)
            {
                Variant? variant;
                if (input.Variant.Id is not null)
                {
                    variant = Variants.FindById(input.Variant.Id.Value);
                    if (variant is null)
                        throw new ArgumentException($"variant with id {input.Variant.Id} not found");
                }
                else if (input.Variant.Characteristic is not null && input.Variant.Parent is not null)
                {
                    variant = Variants.Query()
                        .Include(x => x.Parent)
                        .Where(x => x.Characteristics["DE"].Content == input.Variant.Characteristic)
                        .Where(x => x.Parent.EAN == input.Variant.Parent.EAN)
                        .FirstOrDefault();
                    if (variant is null)
                        throw new ArgumentException($"variant not found");
                }
                else throw new ArgumentException("variant id or variant characteristic with parent is required");
                switch (input.Field.ToLower())
                {
                    case "characteristic":
                        variant.Characteristics[input.Language.ToUpper()] = translation = new Translation
                        {
                            Variant = variant,
                            Content = input.Content,
                            CreatedAt = DateTime.UtcNow,
                            CreatedBy = user,
                            Field = "characteristic",
                            Language = input.Language.ToUpper(),
                            ManuallyTranslated = true,
                        };
                        break;
                    default: throw new ArgumentException($"variant has no field {input.Field}");
                }
                variant.LastChangedAt = DateTime.UtcNow;
                variant.LastChangedBy = user;
                Variants.Update(variant);
            }
            else throw new ArgumentException("translation is not bound to article or variant");
            return translation;
        }
    }
}
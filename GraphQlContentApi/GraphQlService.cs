using System;
using System.Threading.Tasks;
using System.Text;
using MaxLib.WebServer;
using GraphQL;
using GraphQL.Types;
using GraphQL.SystemTextJson;
using System.Text.Json;
using System.Collections.Generic;
using System.Linq;

namespace GraphQlContentApi
{
    public class GraphQlService : WebService
    {
        private class Request
        {
            public string Query { get; set; } = "";

            public string? OperationName { get; set; }

            public Inputs? Variables { get; set; }
        }

        const string queryKey = nameof(GraphQlService) + ":query";

        readonly GraphQL.Types.Schema schema;

        public GraphQlService()
            : base(ServerStage.CreateDocument)
        {
            schema = new GraphQL.Types.Schema
            {
                Query = new Schema.ServiceQuery(),
                Mutation = new Schema.ServiceMutation(),
            };
        }

        public override bool CanWorkWith(WebProgressTask task)
        {
            if (!task.Request.Location.IsUrl(new[] { "graphql" })
                || task.Request.Post.Data is not MaxLib.WebServer.Post.UnknownPostData data
            )
                return false;
            if (data.MimeType == MimeType.ApplicationJson)
            {
                var doc = JsonDocument.Parse(data.Data);
                var request = new Request
                {
                    Query = doc.RootElement.GetProperty("query").GetString() ?? "",
                    OperationName = doc.RootElement.TryGetProperty("operationName", out JsonElement node)
                        ? node.GetString()
                        : null,
                };
                if (doc.RootElement.TryGetProperty("variables", out node) 
                    && node.ValueKind == JsonValueKind.Object)
                {
                    var input = new Dictionary<string, object>();
                    foreach (var entry in node.EnumerateObject())
                    {
                        switch (entry.Value.ValueKind)
                        {
                            case JsonValueKind.String: 
                                input.Add(entry.Name, entry.Value.GetString() ?? "");
                                break;
                            case JsonValueKind.Number:
                                input.Add(entry.Name, entry.Value.GetDouble());
                                break;
                        }
                    }
                    request.Variables = new Inputs(input);
                }
                task.Document.Information[queryKey] = request;
            }
            else
            {
                var query = Encoding.UTF8.GetString(data.Data.ToArray());
                task.Document.Information[queryKey] = new Request
                {
                    Query = query,
                };
            }
            return true;
        }

        public override async Task ProgressTask(WebProgressTask task)
        {
            var query = (Request)task.Document.Information[queryKey]!;
            var context = new Dictionary<string, object?>();
            var json = await schema.ExecuteAsync(o => 
            {
                o.Query = query.Query;
                o.Inputs = query.Variables;
                o.OperationName = query.OperationName;
                o.UserContext = context;
            });

            if (context.TryGetValue("warning", out object? warning))
            {
                var sb = new System.Text.StringBuilder(json);
                var index = json.LastIndexOf('}');
                sb.Remove(index, json.Length - index);
                sb.Append(",\"warning\": \"");
                sb.Append(warning?.ToString()?.Replace("\"", "\\\"") ?? "");
                sb.Append("\"}");
                json = sb.ToString();
            }

            task.Document.DataSources.Add(new HttpStringDataSource(json)
            {
                MimeType = MimeType.ApplicationJson,
            });
        }
    }
}
using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class ServiceMutation : ObjectGraphType
    {
        public ServiceMutation()
        {
            Field<NonNullGraphType<ArticleType>>(
                "addArticle",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ArticleInputType>>
                    {
                        Name = "article",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var article = context.GetArgument<ArticleInput>("article");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("addArticle should be called with a user.");
                    return Program.DB.AddArticle(article ?? new ArticleInput(), user);
                }
            );
            Field<NonNullGraphType<ArticleType>>(
                "changeArticle",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ArticleInputChangeType>>
                    {
                        Name = "article",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var article = context.GetArgument<ArticleInput>("article");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("changeArticle should be called with a user.");
                    if (article is null)
                        return null;
                    return Program.DB.ChangeArticle(article, user);
                }
            );
            Field<NonNullGraphType<VariantType>>(
                "addVariant",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VariantInputType>>
                    {
                        Name = "variant",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var variant = context.GetArgument<VariantInput>("variant");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("addVariant should be called with a user.");
                    if (variant is null)
                        return null;
                    return Program.DB.AddVariant(variant, user);
                }
            );
            Field<NonNullGraphType<ArticleType>>(
                "deleteVariant",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VariantInputRefType>>
                    {
                        Name = "variant",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var variant = context.GetArgument<VariantInput>("variant");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("deleteVariant should be called with a user.");
                    context.AddWarning("deleteVariant will be removed. Use deleteVariantById instead.");
                    if (variant is null)
                        return null;
                    return Program.DB.DeleteVariant(variant, user);
                }
            );
            Field<NonNullGraphType<ArticleType>>(
                "deleteVariantById",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IntGraphType>>
                    {
                        Name = "variantId",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var variantId = context.GetArgument<int>("variantId");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("deleteVariantById should be called with a user.");
                    return Program.DB.DeleteVariant(variantId, user);
                }
            );
            Field<NonNullGraphType<TranslationType>>(
                "addTranslation",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<TranslationInputType>>
                    {
                        Name = "translation",
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "user",
                    }
                ),
                resolve: context =>
                {
                    var translation = context.GetArgument<TranslationInput>("translation");
                    var user = context.GetArgument<string?>("user");
                    if (user is null)
                        context.AddWarning("addTranslation should be called with a user.");
                    if (translation is null)
                        return null;
                    return Program.DB.AddTranslation(translation, user);
                }
            );
        }
    }
}
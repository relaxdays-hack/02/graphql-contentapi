using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using LiteDB;

namespace GraphQlContentApi.Schema
{
    public class Article
    {
        [BsonId]
        public ObjectId Id { get; set; } = new ObjectId();

        public string EAN { get; set; } = "";

        public Dictionary<string, Translation> Titles { get; set; }
            = new Dictionary<string, Translation>();
        
        public Dictionary<string, Translation> Descriptions { get; set; }
            = new Dictionary<string, Translation>();

        public float Price { get; set; }

        // [BsonRef("variants")]
        public List<Variant> Variants { get; set; } = new List<Variant>();

        public string? CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UnixEpoch;

        public string? LastChangedBy { get; set; }

        public DateTime LastChangedAt { get; set; } = DateTime.UnixEpoch;

        [BsonIgnore]
        public bool Complete => Price != 0 
            && Titles.ContainsKey("DE") && Titles["DE"].Content != "" 
            && Descriptions.ContainsKey("DE") && Descriptions["DE"].Content != "";
    }

    public class ArticleInput
    {
        public string EAN { get; set; } = "";

        public string? Title { get; set; } = "";

        public string? Description { get; set; } = "";

        public float? Price { get; set; }
    }


    public class ArticleType : ObjectGraphType<Article>
    {
        public ArticleType()
        {
            Field<NonNullGraphType<StringGraphType>>("ean");
            Field<NonNullGraphType<StringGraphType>>("title",
                resolve: context =>
                {
                    context.AddWarning("The title attribute will be removed. Use titleDE instead.");
                    return context.Source!.Titles.TryGetValue("DE", out Translation? translation) ?
                        translation.Content : "";
                }
            );
            foreach (var lang in Program.Languages)
                AddTitleField(lang);
            Field<NonNullGraphType<StringGraphType>>("description",
                resolve: context =>
                {
                    context.AddWarning("The description attribute will be removed. Use descriptionDE instead.");
                    return context.Source!.Descriptions.TryGetValue("DE", out Translation? translation) ?
                        translation.Content : "";
                }
            );
            foreach (var lang in Program.Languages)
                AddDescriptionField(lang);
            Field<NonNullGraphType<FloatGraphType>>(
                "price",
                resolve: context =>
                {
                    context.AddWarning("The price attribute will be removed. Use priceEUR instead.");
                    return context.Source!.Price;
                }
            );
            Field<ListGraphType<NonNullGraphType<VariantType>>>("variants");
            Field<StringGraphType>("createdBy");
            Field<NonNullGraphType<DateTimeGraphType>>("createdAt");
            Field<StringGraphType>("lastChangedBy");
            Field<NonNullGraphType<DateTimeGraphType>>("lastChangedAt");
            Field<NonNullGraphType<BooleanGraphType>>("complete");
            Field<NonNullGraphType<FloatGraphType>>(
                "priceEUR",
                resolve: context => context.Source!.Price
            );
            foreach (var (key, _) in Program.Conversion)
                AddCurrencyField(key);
        }

        private void AddTitleField(string lang)
        {
            Field<NonNullGraphType<TranslationType>>(
                $"title{lang}",
                resolve: context =>
                {
                    if (context.Source!.Titles.TryGetValue(lang, out Translation? translation))
                        return translation;
                    // do translation
                    var source = context.Source!.Titles["DE"];
                    return source.TranslateAsync(lang).Result;
                }
            );
        }

        private void AddDescriptionField(string lang)
        {
            Field<NonNullGraphType<TranslationType>>(
                $"description{lang}",
                resolve: context =>
                {
                    if (context.Source!.Descriptions.TryGetValue(lang, out Translation? translation))
                        return translation;
                    // do translation
                    var source = context.Source!.Descriptions["DE"];
                    return source.TranslateAsync(lang).Result;
                }
            );
        }

        private void AddCurrencyField(string currency)
        {
            Field<NonNullGraphType<FloatGraphType>>(
                $"price{currency}",
                resolve: context =>
                {
                    var exchange = Program.Conversion[currency];
                    return context.Source!.Price * exchange;
                }
            );
        }
    }

    public class ArticleInputType : InputObjectGraphType<ArticleInput>
    {
        public ArticleInputType()
        {
            Field<NonNullGraphType<StringGraphType>>("ean");
            Field<NonNullGraphType<StringGraphType>>("title");
            Field<NonNullGraphType<StringGraphType>>("description");
            Field<NonNullGraphType<FloatGraphType>>("price");
        }
    }

    public class ArticleInputChangeType : InputObjectGraphType<ArticleInput>
    {
        public ArticleInputChangeType()
        {
            Field<NonNullGraphType<StringGraphType>>("ean");
            Field<StringGraphType>("title");
            Field<StringGraphType>("description");
            Field<FloatGraphType>("price");
        }
    }

    public class ArticleInputRefType : InputObjectGraphType<ArticleInput>
    {
        public ArticleInputRefType()
        {
            Field<NonNullGraphType<StringGraphType>>("ean");
        }
    }
}
using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class StringOperationsInput
    {
        public string? Eq { get; set; }

        public string? Contains { get; set; }

        public bool Filter(string value)
        {
            if (Eq is not null && value != Eq)
                return false;
            if (Contains is not null && !value.Contains(Contains))
                return false;
            return true;
        }
    }

    [GraphQLMetadata(nameof(StringOperationsInput))]
    public class StringOperationsInputType : InputObjectGraphType<StringOperationsInput>
    {
        public StringOperationsInputType()
        {
            Field<StringGraphType>("eq");
            Field<StringGraphType>("contains");
        }
    }
}
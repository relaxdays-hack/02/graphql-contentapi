using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class PageInfo
    {
        public bool HasNextPage { get; set; }

        public bool HasPreviousPage { get; set; }
    }

    public class PageInfoType : ObjectGraphType<PageInfo>
    {
        public PageInfoType()
        {
            Field<NonNullGraphType<BooleanGraphType>>("hasNextPage");
            Field<NonNullGraphType<BooleanGraphType>>("hasPreviousPage");
        }
    }
}
using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public enum Order
    {
        Asc,
        Desc
    }

    public class OrderType : EnumerationGraphType<Order>
    {

    }
}
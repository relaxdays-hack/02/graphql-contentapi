using System;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using LiteDB;

namespace GraphQlContentApi.Schema
{
    public class Translation
    {
        [BsonRef("articles")]
        public Article? Article { get; set; }

        [BsonRef("variants")]
        public Variant? Variant { get; set; }

        public string Field { get; set; } = "";

        public string Language { get; set; } = "de";

        public string Content { get; set; } = "";

        public DateTime CreatedAt { get; set; } = DateTime.UnixEpoch;

        public string? CreatedBy { get; set; }

        public bool ManuallyTranslated { get; set; } = true;

        public async Task<Translation> TranslateAsync(string target)
        {
            var wc = new System.Net.WebClient();
            using var m = new System.IO.MemoryStream();
            using var w = new System.Text.Json.Utf8JsonWriter(m);
            w.WriteStartObject();
            w.WriteString("q", Content);
            w.WriteString("source", "de");
            w.WriteString("target", target.ToLower());
            w.WriteEndObject();
            await w.FlushAsync().ConfigureAwait(false);

            var result = await wc.UploadDataTaskAsync("http://libretranslate:5000/translate", m.ToArray())
                .ConfigureAwait(false);
            var doc = System.Text.Json.JsonDocument.Parse(result);
            var content = doc.RootElement.GetProperty("translatedText").GetString() ?? "";
            return new Translation
            {
                Article = Article,
                Content = content,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = null,
                Field = Field,
                Language = target.ToUpper(),
                ManuallyTranslated = false,
                Variant = Variant,
            };
        }
    }

    public class TranslationInput
    {
        public ArticleInput? Article { get; set; }

        public VariantInput? Variant { get; set; }

        public string Field { get; set; } = "";

        public string Language { get; set; } = "de";

        public string Content { get; set; } = "";
    }

    public class TranslationType : ObjectGraphType<Translation>
    {
        public TranslationType()
        {
            Field<ArticleType>("article");
            Field<VariantType>("variant");
            Field<NonNullGraphType<StringGraphType>>("field");
            Field<NonNullGraphType<StringGraphType>>("language");
            Field<NonNullGraphType<StringGraphType>>("content");
            Field<NonNullGraphType<DateTimeGraphType>>("createdAt");
            Field<StringGraphType>("createdBy");
            Field<NonNullGraphType<BooleanGraphType>>("manuallyTranslated");
        }
    }

    public class TranslationInputType : InputObjectGraphType<TranslationInput>
    {
        public TranslationInputType()
        {
            Field<ArticleInputRefType>("article");
            Field<VariantInputRefType>("variant");
            Field<NonNullGraphType<StringGraphType>>("field");
            Field<NonNullGraphType<StringGraphType>>("content");
            Field<NonNullGraphType<StringGraphType>>("language");
        }
    }
}
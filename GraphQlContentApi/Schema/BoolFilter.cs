using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class BoolFilter
    {
        public bool Eq { get; set; }

        public bool Filter(bool value)
        {
            return value == Eq;
        }
    }

    public class BoolFilteType : InputObjectGraphType<BoolFilter>
    {
        public BoolFilteType()
        {
            Field<BooleanGraphType>("eq");
        }
    }
}
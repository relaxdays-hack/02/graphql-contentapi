using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class Pagination<T>
    {
        public List<T> Items { get; set; } = new List<T>();

        public PageInfo PageInfo { get; set; } = new PageInfo();

        public int TotalCount { get; set; }
    }

    public class PaginationType<T, TT> : ObjectGraphType<Pagination<T>>
        where TT : IGraphType
    {
        public PaginationType()
        {
            Field<ListGraphType<NonNullGraphType<TT>>>("items");
            Field<PageInfoType>("pageInfo");
            Field<IntGraphType>("totalCount");
        }
    }
}
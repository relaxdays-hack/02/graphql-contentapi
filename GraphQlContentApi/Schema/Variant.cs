using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using LiteDB;

namespace GraphQlContentApi.Schema
{
    public class Variant
    {
        [BsonId]
        public int Id { get; set; }

        public Dictionary<string, Translation> Characteristics { get; set; }
            = new Dictionary<string, Translation>();

        [BsonRef("articles")]
        public Article Parent { get; set; } = new Article();

        public string? CreatedBy { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UnixEpoch;

        public string? LastChangedBy { get; set; }

        public DateTime LastChangedAt { get; set; } = DateTime.UnixEpoch;
    }

    public class VariantInput
    {
        public int? Id { get; set; }

        public string? Characteristic { get; set; } = "";
        
        public ArticleInput? Parent { get; set; } = new ArticleInput();
    }

    [GraphQLMetadata(nameof(Variant))]
    public class VariantType : ObjectGraphType<Variant>
    {
        public VariantType()
        {
            Field<NonNullGraphType<StringGraphType>>("characteristic",
                resolve: context =>
                {
                    context.AddWarning("The characteristic attribute will be removed. Use characteristicDE instead.");
                    return context.Source!.Characteristics.TryGetValue("DE", out Translation? translation) ?
                        translation.Content : "";
                }
            );
            foreach (var lang in Program.Languages)
                AddCharacterisicField(lang);
            Field<NonNullGraphType<ArticleType>>("parent",
                resolve: context =>
                {
                    return Program.DB.GetArticle(context.Source!.Parent.Id);
                }
            );
            Field<StringGraphType>("createdBy");
            Field<NonNullGraphType<DateTimeGraphType>>("createdAt");
            Field<StringGraphType>("lastChangedBy");
            Field<NonNullGraphType<DateTimeGraphType>>("lastChangedAt");
        }

        private void AddCharacterisicField(string lang)
        {
            Field<NonNullGraphType<TranslationType>>(
                $"characteristic{lang}",
                resolve: context =>
                {
                    if (context.Source!.Characteristics.TryGetValue(lang, out Translation? translation))
                        return translation;
                    // do translation
                    var source = context.Source!.Characteristics["DE"];
                    return source.TranslateAsync(lang).Result;
                }
            );
        }
    }

    public class VariantInputType : InputObjectGraphType<VariantInput>
    {
        public VariantInputType()
        {
            Field<NonNullGraphType<StringGraphType>>("characteristic");
            Field<NonNullGraphType<ArticleInputRefType>>("parent");
        }
    }

    public class VariantInputRefType : InputObjectGraphType<VariantInput>
    {
        public VariantInputRefType()
        {
            Field<StringGraphType>("characteristic");
            Field<IntGraphType>("id");
            Field<ArticleInputRefType>("parent");
        }
    }
}
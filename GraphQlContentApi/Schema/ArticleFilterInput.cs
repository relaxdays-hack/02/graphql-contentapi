using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using System.Linq;

namespace GraphQlContentApi.Schema
{
    public class ArticleFilterInput
    {
        public List<ArticleFilterInput>? And { get; set; }

        public List<ArticleFilterInput>? Or { get; set; }

        public StringOperationsInput? Ean { get; set; }

        public StringOperationsInput? Title { get; set; }

        public StringOperationsInput? Description { get; set; }

        public ComparableDecimalOperationFilterInput? Price { get; set; }

        public BoolFilter? Complete { get; set; }

        public bool Filter(Article article)
        {
            if (And is not null)
                if (!And.All(x => x.Filter(article)))
                    return false;
            if (Or is not null)
                if (!Or.Any(x => x.Filter(article)))
                    return false;
            if (Ean is not null && !Ean.Filter(article.EAN))
                return false;
            if (Title is not null && !Title.Filter(article.Titles["DE"].Content))
                return false;
            if (Description is not null && !Description.Filter(article.Descriptions["DE"].Content))
                return false;
            if (Price is not null && !Price.Filter(article.Price))
                return false;
            return true;
        }
    }

    [GraphQLMetadata(nameof(ArticleFilterInput))]
    public class ArticleFilterInputType : InputObjectGraphType<ArticleFilterInput>
    {
        public ArticleFilterInputType()
        {
            Field<ListGraphType<NonNullGraphType<ArticleFilterInputType>>>("and");
            Field<ListGraphType<NonNullGraphType<ArticleFilterInputType>>>("or");
            Field<StringOperationsInputType>("ean");
            Field<StringOperationsInputType>("title");
            Field<StringOperationsInputType>("description");
            Field<ComparableDecimalOperationFilterInputType>("price");
            Field<BoolFilteType>("complete");
        }
    }
}
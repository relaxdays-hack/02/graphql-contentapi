using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class ComparableDecimalOperationFilterInput
    {
        public float? Eq { get; set; }

        public float? Gte { get; set; }

        public float? Lte { get; set; }

        public bool Filter(float value)
        {
            if (Eq is not null && value != Eq.Value)
                return false;
            if (Gte is not null && value < Gte.Value)
                return false;
            if (Lte is not null && value > Lte.Value)
                return false;
            return true;
        }
    }

    [GraphQLMetadata(nameof(ComparableDecimalOperationFilterInput))]
    public class ComparableDecimalOperationFilterInputType 
        : InputObjectGraphType<ComparableDecimalOperationFilterInput>
    {
        public ComparableDecimalOperationFilterInputType()
        {
            Field<FloatGraphType>("eq");
            Field<FloatGraphType>("gte");
            Field<FloatGraphType>("let");
        }
    }
}
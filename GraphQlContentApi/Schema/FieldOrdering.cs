using System;
using GraphQL;
using GraphQL.Types;

namespace GraphQlContentApi.Schema
{
    public class FieldOrdering
    {
        public Order? Ean { get; set; }

        public Order? Title { get; set; }

        public Order? Description { get; set; }

        public Order? Price { get; set; }
    }

    [GraphQLMetadata(nameof(FieldOrdering))]
    public class FieldOrderingType : InputObjectGraphType<FieldOrdering>
    {
        public FieldOrderingType()
        {
            Field<OrderType>("ean");
            Field<OrderType>("title");
            Field<OrderType>("description");
            Field<OrderType>("price");
        }
    }
}
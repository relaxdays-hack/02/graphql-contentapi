using System;
using GraphQL;
using GraphQL.Types;
using GraphQL.SystemTextJson;
using System.Collections.Generic;

namespace GraphQlContentApi.Schema
{
    public class ServiceQuery : ObjectGraphType
    {
        public ServiceQuery()
        {
            Field<ListGraphType<ArticleType>>(
                "articles",
                arguments: new QueryArguments(
                    new QueryArgument<FieldOrderingType>
                    {
                        Name = "order",
                    },
                    new QueryArgument<ArticleFilterInputType>
                    {
                        Name = "where",
                    }
                ),
                resolve: context => 
                { 
                    var order = context.GetArgument<FieldOrdering>("order");
                    var where = context.GetArgument<ArticleFilterInput>("where");
                    context.AddWarning("articles is deprecated. Switch to paginatedArticles");
                    return Program.DB.FindArticles(
                        order ?? new FieldOrdering(),
                        where ?? new ArticleFilterInput()
                    );
                }
            );

            Field<PaginationType<Article, ArticleType>>(
                "paginatedArticles",
                arguments: new QueryArguments(
                    new QueryArgument<FieldOrderingType>
                    {
                        Name = "order",
                    },
                    new QueryArgument<ArticleFilterInputType>
                    {
                        Name = "where",
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "skip",
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "take",
                    }
                ),
                resolve: context => 
                { 
                    var order = context.GetArgument<FieldOrdering>("order");
                    var where = context.GetArgument<ArticleFilterInput>("where");
                    var skip = context.GetArgument<int?>("skip");
                    var take = context.GetArgument<int?>("take");
                    return Program.DB.FindPagination(
                        skip,
                        take,
                        Program.DB.FindArticles(
                            order ?? new FieldOrdering(),
                            where ?? new ArticleFilterInput()
                        )
                    );
                }
            );
        }
    }
}
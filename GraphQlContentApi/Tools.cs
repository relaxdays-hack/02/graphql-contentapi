using System.Collections.Generic;
using GraphQL;
using LiteDB;

namespace GraphQlContentApi
{
    public static class Tools
    {
        public static void AddWarning<T>(this IResolveFieldContext<T> context, string warning)
        {
            const string key = "warning";
            var user = context.UserContext;
            if (user.TryGetValue(key, out object? existing))
                user[key] = $"{existing}\\n{warning}";
            else user[key] = warning;
        }

        public static ILiteQueryable<Schema.Article> Order(
            this ILiteQueryable<Schema.Article> query, 
            Schema.FieldOrdering ordering
        )
        {
            query = ordering.Price switch
            {
                Schema.Order.Asc => query.OrderBy(x => x.Price),
                Schema.Order.Desc => query.OrderByDescending(x => x.Price),
                _ => query,
            };
            query = ordering.Description switch
            {
                Schema.Order.Asc => query.OrderBy(x => x.Descriptions["DE"].Content),
                Schema.Order.Desc => query.OrderByDescending(x => x.Descriptions["DE"].Content),
                _ => query,
            };
            query = ordering.Title switch
            {
                Schema.Order.Asc => query.OrderBy(x => x.Titles["DE"].Content),
                Schema.Order.Desc => query.OrderByDescending(x => x.Titles["DE"].Content),
                _ => query,
            };
            query = ordering.Ean switch
            {
                Schema.Order.Asc => query.OrderBy(x => x.EAN),
                Schema.Order.Desc => query.OrderByDescending(x => x.EAN),
                _ => query
            };
            return query;
        }
    }
}
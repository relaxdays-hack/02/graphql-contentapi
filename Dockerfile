FROM mcr.microsoft.com/dotnet/sdk:5.0 as builder
WORKDIR /src
COPY ./GraphQlContentApi ./GraphQlContentApi
# COPY ./GraphQlContentApi.Test ./GraphQlContentApi.Test
COPY ./graphql-contentapi.sln ./
RUN dotnet build --nologo -c RELEASE \
        GraphQlContentApi/GraphQlContentApi.csproj && \
    dotnet publish --nologo -c RELEASE -o /app \
        GraphQlContentApi/GraphQlContentApi.csproj

FROM mcr.microsoft.com/dotnet/runtime:5.0
WORKDIR /app
COPY --from=builder /app /app
ENV IS_DOCKER=1
EXPOSE 80
CMD [ "dotnet", "/app/GraphQlContentApi.dll" ]
